var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';

const game = startGame();
function startGame() {
  const game = {
    gameField: [],
    fieldLength: 9,
    filled: 0,
    current: 1,
    size: 3,
    end: false,
    AI: false,
    playerType: 1
  };
  initField(game);
  return game;
}

function initField(game) {
  game.size = +prompt('Введите размер поля', 3);
  if (!game.size) {
    game.size = 3;
  }
  game.fieldLength = game.size ** 2;
  game.AI = confirm('Играть с ИИ ?');
  if (game.AI) {
    game.playerType = +confirm('Вы играете X ?');
  }
  for (let i = 0; i < game.size; i++) {
    game.gameField[i] = new Array(game.size).fill(EMPTY);
  }
  renderGrid(game.size);
  if (game.AI && !game.playerType) {
    AIMove(game);
  }
}

function getAiPos(game) {
  const avialableMoves = [];
  for (let i = 0; i < game.size; i++) {
    for (let j = 0; j < game.size; j++) {
      if (game.gameField[i][j] === EMPTY) {
        avialableMoves.push({ 'i': i, 'j': j });
      }
    }
  }
  if (game.end || !avialableMoves.length) {
    return { 'i': 0, 'j': 0 };
  }
  const index = Math.floor(Math.random() * avialableMoves.length);
  return avialableMoves[index];
}

function AIMove(game) {
  setTimeout(() => {
    const pos = getAiPos(game);
    makeMove(game, pos.i, pos.j)
  }, 1000);
}

/* обработчик нажатия на клетку */
function cellClickHandler(row, col) {
  // Пиши код тут
  console.log(`Clicked on cell: ${row}, ${col}`);
  if (!game.AI || game.playerType === game.current) {
    makeMove(game, row, col);
    if (game.AI) {
      AIMove(game);
    }
  }
}

/* Пользоваться методом для размещения символа в клетке так:
     renderSymbolInCell(ZERO, row, col);
  */
function makeMove(game, row, col) {
  if (!game.end && game.gameField[row][col] === EMPTY) {
    const currentType = game.current ? CROSS : ZERO;
    game.gameField[row][col] = currentType;
    renderSymbolInCell(game.gameField[row][col], row, col);
    game.filled++;
    const message = checkWin(game, col, row) ? ` ${currentType}` : game.filled === game.fieldLength ? 'а дружба' : '';
    if (message) {
      showMessage(`Победил${message}`);
      game.end = true;
    }
    game.current ^= 1;
  }
}
function checkWin(game, col, row) {
  const type = game.current ? CROSS : ZERO;
  const winCombination = [];
  let win = false;
  const parameters = getParameters(col, row, game.size);
  for (let i = 0; i < parameters.length; i++) {
    if (checkWinCombination(game, winCombination, type, parameters[i])) {
      win = true;
      break;
    }
  }
  if (win) {
    for (let i = 0; i < game.size; i++) {
      renderSymbolInCell(type, winCombination[i].row, winCombination[i].col, 'red');
    }
  }
  return win;
}

function getParameters(col, row, size) {
  const parameters = [{ col: col, row: 0, dc: 0, dr: 1 }, { col: 0, row: row, dc: 1, dr: 0 }];
  if (col === row)
    parameters.push({ col: 0, row: 0, dc: 1, dr: 1 });
  if (col + row + 1 === size)
    parameters.push({ col: game.size - 1, row: 0, dc: -1, dr: 1 });
  return parameters;
}

function checkWinCombination(game, winCombination, type, prm) {
  const { col, row, dc, dr } =  {...prm} ;
  winCombination.splice(0);
  let win = true;
  for (let k = 0; k < game.size; k++) {
    const i=row + dr*k,j=col + dc*k;
    win &= game.gameField[i][j] === type;
    winCombination.push({ 'row': i, 'col': j});
  }
  return win;
}
/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler() {
  game.end = false;
  game.current = 1;
  game.filled = 0;
  showMessage('');
  initField(game);
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid(dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell(symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell(row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin() {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw() {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell(row, col) {
  findCell(row, col).click();
}
